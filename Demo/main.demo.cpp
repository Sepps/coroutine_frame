#include "coroutine_frame.hpp"
#include <iostream>
#include <string>
#include <cxxabi.h>  // needed for abi::__cxa_demangle

template <typename T> struct Id{ using type = T; };
template <typename T>
std::string demangle(Id<T>)
{
  int status;    
  char *ret = abi::__cxa_demangle(typeid(Id<T>).name(), 0, 0, &status);  
  std::string output{ ret };
  free(ret);
  output.pop_back();
  return output.substr(3);
}

struct body
{
	using int_ref = int&;
	int_ref a, b, c;

	[[gnu::noinline]] void print()
	{
		std::cout << "a = " << a << "\n";
		std::cout << "b = " << b << "\n";
		std::cout << "c = " << c << "\n";
	}

	static void s1(body & b)
	{
		b.a = 4;
		b.print();
	}

	static void s2(body & b)
	{
		b.b = 5;
		b.print();
	}

	static void s3(body & b)
	{
		b.c = 6;
		b.print();
	}

	using fn_type = void(*)(body&);
	inline static fn_type states[]
	{
		s1, s2, s3
	};

	static constexpr size_t state_count = std::size(states);

	void resume(size_t state) noexcept
	{
		states[state](*this);
	}
};

struct task
{
	struct promise_type
	{
		template <typename ... Args>
		void* operator new(size_t len, Args && ... args) noexcept
		{
			std::cout << "Promise operator new\nargs = ";
			((std::cout << demangle(Id<Args>{}) << "[" << args << "]" << " "), ...);
			std::cout << "\n\n";
			return ::operator new(len);
		}

		void operator delete(void* addr) noexcept
		{
			std::cout << "Promise operator delete\n\n";
			::operator delete(addr);
		}

		template <typename ... Args>
		promise_type(Args && ... args)
		{
			std::cout << "Promise constructor\nargs = ";
			((std::cout << demangle(Id<Args>{}) << "[" << args << "]" << " "), ...);
			std::cout << "\n\n";
		}
	};
};

void using_unerased_frame() 
{
	std::puts("Using coroutine_frame");
	sstd::coroutine_frame<sstd::coroutine_body<body, task, int, int, int>> frame{ 1, 2, 3 };

	// Using non-erased frame.
	while(not frame.done())
	{
		frame.resume();
	}
	std::puts("\n");
}

void using_coroutine_handle_with_promise()
{
	std::puts("Using coroutine_handle<promise>");
	sstd::coroutine_frame<sstd::coroutine_body<body, task, int, int, int>> frame{ 7, 8, 9 };
	sstd::coroutine_handle<typename sstd::coroutine_traits<task>::promise_type> handle{ frame };

	// Using non-erased frame.
	while(not handle.done())
	{
		handle.resume();
	}
	std::puts("\n");
}

void using_erased_coroutine_handle_with_allocation()
{
	std::puts("Using coroutine_handle<> with ref parameters and allocation");
	int a = 8;
	auto f = sstd::new_coroutine_frame<body, task>(7, a, 9);
	sstd::coroutine_handle<> handle{ *f };

	// Using non-erased frame.
	while(not handle.done())
	{
		handle.resume();
	}
	std::puts("\n");
}

int main()
{
    using_unerased_frame();
    using_coroutine_handle_with_promise();
    using_erased_coroutine_handle_with_allocation();
}