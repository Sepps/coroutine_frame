#include <type_traits>
#include <concepts>
#include <utility>
#include <tuple>
#include <cstddef>
#include <cstdint>
#include <compare>
#include <memory>

#define constexpr_fold(x) (__builtin_constant_p(x) ? (x) : (x))
namespace sstd
{
	namespace helpers
	{
		template <typename TopObj, auto F>
		class MemFnImpl
		{
		private:
			template <bool is_noexcept, bool is_const, typename Obj, typename Ret, typename ... Args>
			struct Base
			{
				using void_ptr = std::conditional_t<is_const, void const *, void *>;
				using obj_ptr  = std::conditional_t<is_const,  TopObj const *,  TopObj *>;

				[[gnu::always_inline]] inline static constexpr Ret Invoke(void_ptr ptr, Args ... args) noexcept(is_noexcept)
				{
					return (constexpr_fold(reinterpret_cast<obj_ptr>(ptr))->*F)(args...);
				}
			};

			template <typename>
			struct Deduce;
			template <typename Obj, typename Ret, typename ... Args>
			struct Deduce<Ret (Obj::*) (Args...)> : Base<false, false, Obj, Ret, Args...> {};
			template <typename Obj, typename Ret, typename ... Args>
			struct Deduce<Ret (Obj::*) (Args...) const> : Base<false, true, Obj, Ret, Args...> {};
			template <typename Obj, typename Ret, typename ... Args>
			struct Deduce<Ret (Obj::*) (Args...) noexcept> : Base<true, false, Obj, Ret, Args...> {};
			template <typename Obj, typename Ret, typename ... Args>
			struct Deduce<Ret (Obj::*) (Args...) const noexcept> : Base<true, true, Obj, Ret, Args...> {};
		public:
			static constexpr auto value = &Deduce<std::decay_t<decltype(F)>>::Invoke;
		};

		template <typename TopObj, auto F>
		constexpr auto MemFn = MemFnImpl<TopObj, F>::value;

		template <typename TopObj, auto F>
		class IsDirectMemFnImpl
		{
		private:
			template <typename FnObj, typename Ret, typename ...>
			struct Base
			{
				static constexpr bool value = std::is_same_v<std::remove_cvref_t<FnObj>, std::remove_cvref_t<TopObj>>;
			};

			template <typename>
			struct Deduce;
			template <typename Obj, typename Ret, typename ... Args>
			struct Deduce<Ret (Obj::*) (Args...)> : Base<Obj, Ret, Args...> {};
			template <typename Obj, typename Ret, typename ... Args>
			struct Deduce<Ret (Obj::*) (Args...) const> : Base<Obj, Ret, Args...> {};
			template <typename Obj, typename Ret, typename ... Args>
			struct Deduce<Ret (Obj::*) (Args...) noexcept> : Base<Obj, Ret, Args...> {};
			template <typename Obj, typename Ret, typename ... Args>
			struct Deduce<Ret (Obj::*) (Args...) const noexcept> : Base<Obj, Ret, Args...> {};
		public:
			static constexpr auto value = Deduce<std::decay_t<decltype(F)>>::value;
		};

		template <typename TopObj, auto F>
		constexpr auto IsDirectMemFn = IsDirectMemFnImpl<TopObj, F>::value;

		template <typename TopObj, auto F>
			requires IsDirectMemFn<TopObj, F>
		constexpr auto DirectMemFn = MemFn<TopObj, F>;

        template <typename T>
        struct remove_rvalue_reference {
            using type = T;
        };

        template <typename T>
        struct remove_rvalue_reference<T&&> {
            using type = T;
        };

        template <typename T>
        using remove_rvalue_reference_t = typename remove_rvalue_reference<T>::type;

        template <typename T>
        constexpr T & ConstRValue(typename std::remove_reference<T>::type & ref) noexcept
        {
            return ref;
        }

        template <typename T>
        constexpr T const & ConstRValue(typename std::remove_reference<T>::type && ref) noexcept
        {
            return ref;
        }
	}

    template <class R, class... Args>
    struct coroutine_traits;

    class coroutine_frame_common
    {
    private:
        using resume_fn_ptr  = void(*)(void*, size_t);

        size_t m_state = 0;
        size_t const m_max_state = 0;
        
        // Unsized, to allow mandatory elision if not odr used.
        resume_fn_ptr   m_resume_fn;
    public:
        template <typename Obj>
        constexpr coroutine_frame_common(Obj&) noexcept
            : m_max_state{ Obj::body_type::body_type::state_count } // TODO: minify
            , m_resume_fn{ helpers::DirectMemFn<Obj, static_cast<void(Obj::*)(size_t)>(&Obj::resume)> }
        {}

		constexpr size_t state() const noexcept { return m_state; }
		constexpr size_t inc_state() noexcept { return m_state++; }
        constexpr const void* address() const noexcept { return static_cast<const void*>(this); }
        constexpr bool done() const noexcept { return m_max_state == state(); }
        constexpr void resume() noexcept { return m_resume_fn(constexpr_fold(const_cast<void*>(address())), inc_state()); }
    };

    template <typename Body, typename Ret, typename ... Args>
    struct coroutine_body
    {
        using body_type = Body;
		using args_type = typename coroutine_traits<Ret, Args...>::args_storage_type;
        union{ Body body; };
        args_type args;

        template <typename ... As>
        constexpr coroutine_body(As && ... args_) noexcept
            : args{ std::forward<As>(args_)... }
        {
            std::apply
            (
                [&](auto && ... args) noexcept
                {
                    std::construct_at(std::addressof(body), std::forward<decltype(args)>(args)...);
                },
                args
            );
        }
    };

    template <typename Promise>
    struct coroutine_common
    {
        coroutine_frame_common frame;
        union{ Promise promise; };

        static constexpr coroutine_frame_common * from_promise_to_frame(Promise * addr) noexcept
        {
            uintptr_t ptr = constexpr_fold(reinterpret_cast<uintptr_t>(addr));
            uintptr_t obj_ptr = ptr - offsetof(coroutine_common, promise);
            return std::addressof(constexpr_fold(reinterpret_cast<coroutine_common*>(obj_ptr))->frame);
        }

        static constexpr Promise * from_frame_to_promise(coroutine_frame_common * addr) noexcept
        {
            uintptr_t ptr = constexpr_fold(reinterpret_cast<uintptr_t>(addr));
            uintptr_t obj_ptr = ptr - offsetof(coroutine_common, promise);
            return std::addressof(constexpr_fold(reinterpret_cast<coroutine_common*>(obj_ptr)));
        }

        constexpr coroutine_frame_common & frame_common() noexcept
        {
            return frame;
        }

        template <typename Args>
        constexpr void construct(Args & args) noexcept
        {
			std::apply
			(
				[&](auto && ... args) noexcept
				{
					if constexpr (requires { Promise{ std::forward<decltype(args)>(args)... }; })
					{
                        std::construct_at(std::addressof(promise), std::forward<decltype(args)>(args)...);
					}
					else
					{
						((void)args, ...);
                        std::construct_at(std::addressof(promise));
					}
				},
				args
			);
        }

        constexpr ~coroutine_common()
        {
            promise.~Promise();
        }
    };

    // Compiler substitutes Body
    template <typename Body>
    class coroutine_frame;

    template <typename Body, typename Ret, typename ... BodyArgs>
    class coroutine_frame<coroutine_body<Body, Ret, BodyArgs...>> : coroutine_common<typename coroutine_traits<Ret, BodyArgs...>::promise_type>
    {
    public:
        using promise_type = typename coroutine_traits<Ret, BodyArgs...>::promise_type;
        using body_type = coroutine_body<Body, Ret, BodyArgs...>;
        using common = coroutine_common<promise_type>;
        using common::frame_common;

        template <typename ... Args>
        static constexpr void* operator new(size_t len, Args && ... args) noexcept
        {
            if constexpr (requires{ promise_type::operator new(len, helpers::ConstRValue<Args>(std::forward<Args>(args))...); })
                return promise_type::operator new(len, helpers::ConstRValue<Args>(std::forward<Args>(args))...);
            else
                return ::operator new(len);
        }

        static constexpr void operator delete(void* addr) noexcept
        {
            if constexpr (requires{ promise_type::operator delete(addr); })
                return promise_type::operator delete(addr);
            else 
                return ::operator delete(addr);
        }

        // Construct promise with promise args, construct common erased base
        template <typename ... Args>
        constexpr coroutine_frame(Args && ... args) noexcept
            : common{ { *this } }
            , body{ std::forward<Args>(args)... }
        {
            common::construct( body.args );
        }
		
		constexpr void resume()
		{
			return body.body.resume(common::frame.inc_state());
		}
		constexpr bool done() const noexcept
		{
			return common::frame.done();
		}

		constexpr promise_type & promise() noexcept
		{
			return common::promise;
		}
    private:
		friend class coroutine_frame_common;

		constexpr void resume(size_t state)
		{
			return body.body.resume(state);
		}

        body_type body;
    };

    using std::nullptr_t;

    template <typename Promise = void>
    class coroutine_handle;

    template <>
    class coroutine_handle<void>
    {
    public:
        constexpr coroutine_handle() noexcept = default;
        constexpr coroutine_handle(std::nullptr_t) noexcept {}
        constexpr coroutine_handle(coroutine_handle &&) noexcept = default;
        constexpr coroutine_handle(coroutine_handle const &) noexcept = default;

		template <typename Body>
        constexpr coroutine_handle(coroutine_frame<Body> & frame) noexcept
            : m_frame{ std::addressof( frame.frame_common() ) }
        {}
    
        constexpr coroutine_handle& operator=(std::nullptr_t) noexcept
        {
            m_frame = nullptr;
            return *this;
        }
        constexpr coroutine_handle& operator=(coroutine_handle &&) noexcept = default;
        constexpr coroutine_handle& operator=(coroutine_handle const &) noexcept = default;

        constexpr const void* address() const noexcept
        {
            return m_frame;
        }

        constexpr void destroy() const
        {
            // TODO: How to handle this in a backward compatible way?
        }

        constexpr void resume() const
        {
            m_frame->resume();
        }

        constexpr void operator()() const
        {
            resume();
        }

        constexpr bool done() const noexcept
        {
            return m_frame->done();
        }

        constexpr bool operator == (coroutine_handle const & rhs) const noexcept
        {
            return m_frame == rhs.m_frame;
        }
        constexpr std::strong_ordering operator <=> (coroutine_handle const & rhs) const noexcept
        {
            return std::compare_three_way{}(m_frame, rhs.m_frame);
        }

        explicit constexpr operator bool() const noexcept
        {
            return m_frame != nullptr;
        } 
    private:
		template <typename>
		friend class coroutine_handle;
        coroutine_frame_common * m_frame = nullptr;
    };

    template <typename Promise>
    class coroutine_handle : coroutine_handle<void>
    {
        using base = coroutine_handle<void>;
        using common = coroutine_common<Promise>;
    public:
        using base::base;
        using base::done;
        using base::resume;
        using base::destroy;
        using base::address;
        using base::operator=;
        using base::operator();
        using base::operator bool;
        using base::operator==;
        using base::operator<=>;

        constexpr Promise& promise() const
        {
            return *common::from_frame_to_promise(m_frame);
        }

        static constexpr coroutine_handle from_address( void* addr ) noexcept
        {
            coroutine_handle output;
            output.m_frame = constexpr_fold(reinterpret_cast<coroutine_frame_common *>(addr));
            return output;
        }

        static constexpr coroutine_handle from_promise( Promise & promise ) noexcept
        {
            coroutine_handle output;
            output.m_frame = common::from_promise_to_frame(std::addressof(promise));
            return output;
        }

        template <typename B>
        static constexpr coroutine_handle from_frame( coroutine_frame<B> & frame ) noexcept
        {
            coroutine_handle output;
            output.m_frame = std::addressof(frame.frame_common());
            return output;
        }
    };

	template <typename Body, typename Ret>
	struct new_coroutine_frame_impl
	{
		template <typename ... Args>
		using frame = sstd::coroutine_frame<sstd::coroutine_body<Body, Ret, Args...>>;

		template <typename ... Args>
		static void frame_deleter(frame<Args...> * ptr)
		{
			frame<Args...>::operator delete(ptr);
		}
		
		template <typename ... Args>
		using output = std::unique_ptr<frame<Args...>, void (*)(frame<Args...>*)>;

		template <typename ... Args>
		constexpr auto operator()(Args && ... args) const noexcept -> output<Args...>
		{
			return 
			{
				new (std::forward<Args>(args)...) frame<Args...>{ std::forward<Args>(args)... },
				&frame_deleter
			};
		}
	};

	template <typename Body, typename Ret>
	inline constexpr new_coroutine_frame_impl<Body, Ret> new_coroutine_frame{};

    template <class R, class... Args>
    struct coroutine_traits
    {
        using promise_type = typename R::promise_type;

        // Requirements:
        // - Support std::apply
        using args_storage_type = std::tuple<helpers::remove_rvalue_reference_t<Args>...>;
    };
}

namespace std
{
    template <class _Tp>
    struct hash;
    template <class _Tp>
    struct hash<sstd::coroutine_handle<_Tp> >
    {
        using arg_type = sstd::coroutine_handle<_Tp>;

        inline size_t operator()(arg_type const& arg) const noexcept
        {
            return hash<void*>()(arg.address());
        }
    };
}

#undef constexpr_fold