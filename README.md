coroutine_body< Promise >
---
Stores arguments and compiler generated data and the coroutine.

coroutine_frame_common
---
A base class of all coroutine_frames allowing for a common pointer type. This is what coroutine_handle stores a pointer to.


coroutine_common< Promise >
---
Stores the coroutine_frame_common and promise.

coroutine_frame< Promise, Body >
---
The top level class that can be used instead of coroutine_handle<Promise>.

Example
---
The most basic example is one that does nothing:
```CPP
struct body
{
    // Compiler responsible for inserting its own implementation defined data here.
    // The type of this data is unspecified.
    // This leaves the door open for future optimisations.

    // This is a compiler generated variable, it annotates the number of suspend points.
    static constexpr size_t state_count = 0;

    // This is a compiler generated function.
    void resume(size_t state) noexcept
    {
        // likely just run from a callback lookup table, or a switch/case etc.
    }
};

struct task
{
    struct promise_type
    {
        // Optionally include operator new and delete.
        // Essentially a normal promise_type body.
    };
};

sstd::coroutine_frame<sstd::coroutine_body<body, task, int, int, int>> frame{ 1, 2, 3 };
sstd::coroutine_handle<> handle{ frame };
```

The advantages of this approach:
1. No need to copy references to the stored parameters, they are accessible without a copy.
2. Allocation is opt-in and HALO not required.
3. No additional storage cost for `std::coroutine_handle<>`.
4. No known backward compaibility problems with `std::coroutine_handle<>`.

There are various customisation points:
1. `operator new`/`operator delete` inside the promise_type for handling allocations. The arguments provided to the coroutine will be passed to the allocators as `lvalues`. `rvalue` references will be converted to `const` `lvalue` references.
2. `sstd::coroutine_traits<R, Args...>` for `promise_type` retrieval and a new `args_storage_type` for the storage of arguments passed to the coroutine.
3. `promise_type` constructor, `lvalue` references to the stored arguments will be provided to the `promise_type`.

To do:
1. If `operator new`/`operator delete` are defined and are noexcept, run `get_return_object_on_allocation_failure` if `nullptr` is returned.
1. Implementation in clang that will generate the body of the `body` class.
2. Write proposal for iso.


Needed:
1. Reviews
2. Ideas
3. Feedback
